FROM openjdk:8-alpine
Volume /tmp
ADD /target/*.jar springbootgitlab-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/springbootgitlab-0.0.1-SNAPSHOT.jar"]
